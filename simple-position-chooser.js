function getUniqueAndOrderedValues(array) {
    var res = [];
    if (array.length > 0) {
        array = array.sort(function (a, b) {
            return a - b
        });
        res.push(array[0]);
        for (var i = 1; i < array.length; i++) {
            if (array[i] != array[i - 1]) {
                res.push(array[i])
            }

        }
    }
    return res;
}

function removeMultiplesOfMultiples(multiplesList) {
    for (var i = 0; i < multiplesList.length - 1; i++) {
        for (var j = i + 1; j < multiplesList.length; j++) {
            if (multiplesList[j] % multiplesList[i] == 0) { //its a multiple
                multiplesList.splice(j, 1);
                j--;
            }
        }
    }
}

function mergeRanges(ranges) {
    for (var i = 0; i < ranges.length; i++) {
        for (var j = 0; j < ranges.length; j++) {
            if (i != j) {

                if (ranges[j].start <= ranges[i].start && ranges[i].start - 1 <= ranges[j].end) {
                    // The minus 1 is because if you have the ranges 2-6 and 7-9 they are equal to 2-9
                    // due to the numbers only are integers so between 6 and 7 theres nothing.
                    //    |_______________| <- j range
                    //
                    //                     |__________|  \
                    //                    |__________|    |
                    //                                     > possible i ranges
                    //             |__________|           |   
                    //        |______| (a)               /
                    //
                    if (ranges[i].end > ranges[j].end) { //Merges the ranges
                        ranges[i].start = ranges[j].start
                        ranges.splice(j, 1);
                        j--;
                        if (j < i) { //If j < i then the current i is now i-1
                            i--;
                        }
                    } else { // (a) if range i is inside range j 
                        ranges.splice(i, 1);
                        i--;
                        j = -1; //Restarts the j cycle because the current i range was removed
                    }
                } else if (ranges[j].start <= ranges[i].end - 1 && ranges[i].end <= ranges[j].end) {
                    ranges[i].end = ranges[j].end
                    ranges.splice(j, 1);
                    j--;
                }

            }

        }
    }
}

function equalOrLargerIntOrDefault(value, equalOrLarger, defaultValue) {
    if (value != undefined) {
        value = Number.parseInt(value);
        if (Number.isSafeInteger(value) && value >= equalOrLarger) {
            return value;
        }
    }
    return defaultValue;
}

function totalFunction(start, end) {
    var positions = [];
    start = equalOrLargerIntOrDefault(start, 0, undefined);
    if (start != undefined) {
        end = equalOrLargerIntOrDefault(end, start + 1, undefined);
        if (end != undefined) {
            for (var i = start; i < end; i++) {
                positions.push(i);
            }
        }
    }
    return positions;
};

function processPositions(str = '*') {
    if (str === '*') {
        return totalFunction;
    } else {
        var aux = str.toString().trim().split(',');
        var isTotal = false // if a some rules combined are able to cover all possible positions 
        var positions = []
        var multiples = [];
        var isEven = false;
        var isOdd = false;
        var rangeMaxStart = undefined;
        var ranges = [];
        for (var option of aux) {
            if (option != '') {
                option = option.trim();
                if (option === 'even') {
                    //If the positions can be even or odd then all values are valid
                    if (isOdd) {
                        isTotal = true;
                        break;
                    }
                    isEven = true;
                } else if (option === 'odd') {
                    //If the positions can be even or odd then all values are valid
                    if (isEven) {
                        isTotal = true;
                        break;
                    }
                    isOdd = true;
                } else if (option.charAt(0) == '/') {
                    var x = equalOrLargerIntOrDefault(option.substring(1), 1, undefined);
                    if (x) {
                        if (x == 1) {
                            isTotal = true;
                            break;
                        } else if (x == 2) {
                            isEven = true;
                            if (isEven && isOdd) {
                                isTotal = true;
                                break;
                            }
                        } else {
                            multiples.push(x);
                        }
                    }
                } else {
                    var x = option.split('-');
                    if (x.length == 1) { //If its a page
                        x = equalOrLargerIntOrDefault(x[0], 0, undefined);
                        if (x != undefined) {
                            positions.push(x);
                        }
                    } else if (x.length == 2) {
                        if (x[0] == '') {
                            x[0] = '0';
                        }
                        x[0] = equalOrLargerIntOrDefault(x[0], 0, undefined);
                        if (x[0] != undefined) {
                            if (x[1] == '') {
                                //If more than a range-max token appears then the one that starts first
                                if (rangeMaxStart) {
                                    if (rangeMaxStart > x[0]) {
                                        if (x[0] == 0) { //If the range-max starts at 0(the first position) then all positions are valid
                                            isTotal = true;
                                            break;
                                        }
                                        rangeMaxStart = x[0];
                                    }
                                } else {
                                    rangeMaxStart = x[0];
                                }
                            } else {
                                x[1] = equalOrLargerIntOrDefault(x[1], 0, undefined);
                                if (x[1] != undefined) {

                                    if (x[0] < x[1]) {
                                        ranges.push({
                                            start: x[0],
                                            end: x[1]
                                        });
                                    } else if (x[0] > x[1]) { //put them in the right order
                                        ranges.push({
                                            start: x[1],
                                            end: x[0]
                                        });
                                    } else {
                                        positions.push(x);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (isTotal) {
            return totalFunction;
        }

        if (multiples.length > 0) {
            if (isEven) {
                multiples.push(2) //This is to remove all even multiples
            }
            multiples = getUniqueAndOrderedValues(multiples);

            removeMultiplesOfMultiples(multiples);
            if (multiples[0] == 2) { // removes the added 2
                multiples.shift();
            }

        }

        if (rangeMaxStart) {
            /*
                Graphical representation of the logic

            |_______________________________________________________________... <- Total

                                            |_______________________________... <- rangeMax
                                                |  |
                                                |__________| <- will be removed because the start is in the rangeMax 
                                                   |
                            |__________| <- Will NOT be merged with the rangeMax although it could be merged with the new rangeMax
                                       |           |
                                    |______________| <- Will be merged with rangeMax
                                       |         
                                    |_______________________________________... <- New rangeMax
            */


            for (var i = 0; i < ranges.length; i++) {
                if (ranges[i].start >= rangeMaxStart) { //Removes ranges that are inside the rangeMax range
                    ranges.splice(i, 1);
                    i--;

                } else if (ranges[i].end >= rangeMaxStart) { // Merges the ranges that are connected to the rangeMax (note: due to order some ranges that coud be connected arent going to be connected)
                    if (ranges[i].start == 0) { //erro aqui
                        isTotal = true;
                        break;
                    }
                    rangeMaxStart = ranges[i].start;
                    ranges.splice(i, 1);
                    i--;
                }
            }
        }

        if (!isTotal) {
            mergeRanges(ranges);
            if (rangeMaxStart) {
                for (var i = 0; i < ranges.length; i++) {
                    if (ranges[i].start >= rangeMaxStart) { //Removes ranges that are inside the rangeMax range
                        ranges.splice(i, 1);
                        i--;
                    } else if (ranges[i].end >= rangeMaxStart - 1) { //checks if one of the final ranges can be merged with the rangeMax
                        if (ranges[i].start == 0) {
                            isTotal = true;
                            break;
                        }
                        rangeMaxStart = ranges[i].start;
                        ranges.splice(i, 1);
                        i--;
                    }
                }
            }
        }

        if (isTotal) {
            return totalFunction;
        }

        //Removes all multiples that are bigger than the rangeMaxStart
        if (rangeMaxStart && multiples.length) {
            for (var i = 0; i < multiples.length; i++) {
                if (multiples[i] >= rangeMaxStart) {
                    multiples.splice(i, multiples.length - i);
                    break;
                }
            }
            if (!multiples.length) {
                positions.push(0) //Due to 0 being a universal multiple (any integer * 0 == 0) and 0 always being lower than the maxRangethe zero is pushed incase all multiples were removed
            }
        }

        positions = getUniqueAndOrderedValues(positions);
        for (var i = 0; i < positions.length; i++) {
            if (rangeMaxStart) { //Removes all positions inside the rangeMax
                if (positions[i] >= rangeMaxStart) {
                    positions.splice(i, 1);
                    i--;
                    continue;
                }
            }
            if (isEven && positions[i] % 2 == 0) { //Removes all even positions 
                positions.splice(i, 1);
                i--;
                continue;
            }
            if (isOdd && positions[i] % 2 == 1) { //Removes all odd positions 
                positions.splice(i, 1);
                i--;
                continue;
            }

            for (var j = 0; j < ranges.length; j++) { //Removes all positions inside a range
                if (ranges[j].start <= positions[i] && positions[i] <= ranges[j].end) {
                    positions.splice(i, 1);
                    i--;
                    continue;
                }
            }

            for (var j = 0; j < multiples.length; j++) { //Removes all positions that are a multiple of a "multiple"
                if (positions[i] % multiples[j] == 0) {
                    positions.splice(i, 1);
                    i--;
                    continue;
                }
            }
        }
        return function (start, end, debug) {
            return calculatePositions(isEven, isOdd, rangeMaxStart, multiples, ranges, positions, start, end, debug);
        };
    }

}

function calculatePositions(isEven, isOdd, rangeMaxStart, multiples, ranges, positions, start, end, debug) {
    if (debug) {
        console.log('------------Processed Rulues------------');
        console.log('isEven: ' + isEven);
        console.log('isOdd: ' + isOdd);
        console.log('rangeMaxStart: ' + rangeMaxStart);
        console.log('multiples: ' + multiples);
        if (ranges.length) {
            var rangesStr = "[" + ranges[0].start + "-" + ranges[0].end;
            for (var i = 1; i < ranges.length; i++) {
                rangesStr += ", " + ranges[i].start + "-" + ranges[i].end;
            }
            rangesStr += "]"
        }
        console.log('ranges: ' + rangesStr);
        console.log('positions: ' + positions);
        console.log('------------Processed Rulues------------');
    }

    var calculatedPositions = [];
    start = equalOrLargerIntOrDefault(start, 0, undefined);
    if (start != undefined) {
        end = equalOrLargerIntOrDefault(end, start + 1, undefined);
        if (end != undefined) {

            for (var i = 0; i < positions.length; i++) {
                if (positions[i] >= start) {
                    if (positions[i] < end) {
                        calculatedPositions.push(positions[i])
                    } else {
                        break; //Due to all of the positions are ordered if one is bigger or equal to the max value then all the next positions are invalid
                    }
                }
            }
            var evenStart;
            var oddStart;
            if (start % 2 == 0) { //start its even
                evenStart = start;
                oddStart = start + 1;

            } else {
                evenStart = start + 1;
                oddStart = start;
            }
            if (isEven) {

                for (var i = evenStart; i < end; i += 2) { //Adds all even numbers
                    calculatedPositions.push(i);
                }
                for (var i = oddStart; i < end; i += 2) { //Checks all odd numbers
                    if (rangeMaxStart && i >= rangeMaxStart) {
                        calculatedPositions.push(i);
                    } else {
                        var found = false;
                        for (var j = 0; j < ranges.length; j++) {
                            if (ranges[j].start <= i && i <= ranges[j].end) {
                                calculatedPositions.push(i);
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            for (var j = 0; j < multiples.length; j++) {
                                if (i % multiples[j] == 0) {
                                    calculatedPositions.push(i);
                                    break;
                                }
                            }
                        }
                    }
                }
            } else if (isOdd) {
                for (var i = oddStart; i < end; i += 2) { //Adds all odd numbers
                    calculatedPositions.push(i);
                }
                for (var i = evenStart; i < end; i += 2) { //Checks all even numbers
                    if (rangeMaxStart && i >= rangeMaxStart) {
                        calculatedPositions.push(i);
                    } else {
                        var found = false;
                        for (var j = 0; j < ranges.length; j++) {
                            if (ranges[j].start <= i && i <= ranges[j].end) {
                                calculatedPositions.push(i);
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            for (var j = 0; j < multiples.length; j++) {
                                if (i % multiples[j] == 0) {
                                    calculatedPositions.push(i);
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                for (var i = start; i < end; i++) { //Checks all numbers
                    if (rangeMaxStart && i >= rangeMaxStart) {
                        calculatedPositions.push(i);
                    } else {
                        var found = false;
                        for (var j = 0; j < ranges.length; j++) {
                            if (ranges[j].start <= i && i <= ranges[j].end) {
                                calculatedPositions.push(i);
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            for (var j = 0; j < multiples.length; j++) {
                                if (i % multiples[j] == 0) {
                                    calculatedPositions.push(i);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return getUniqueAndOrderedValues(calculatedPositions);
}

module.exports.processPositionsString = processPositions;