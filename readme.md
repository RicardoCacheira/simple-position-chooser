# Simple Position Chooser

This package allows for an easy and simple way to choose multiple positions using a string or an array with the desired rules.


### How to use

The way to choose the desired positions is by using any of the rules seperated by a comma in a string or by having the rules in an array. Then you use the generated function with the start position and end position to generate the derised positions.

### Rules

(X and Y represent an non negative integer number)

1. \* : adds all positions. This is a special rule because it cannot be used with any other rule.

1. X : adds the position X.

2. X-Y : adds all positions between X and Y (X and Y included).

3. -X : adds all positions between 0 and X (same as 0-X)

4. X- : adds all positions between X and the last positions (X included)

5. /X : adds all positions that are multiple of X

6. even -> adds all even positions

7. odd -> adds all odd positions

#### Notes
1. Any invalid rule will simply be ignored.
2. Any position will only be added once, even if there are more than one rule defining it.
3. 0 is a univerval multiple
4. The list of desired positions is always ordered.

Example: '1,3,7-9' will adds positions 1,3,7,8 and 9.

### How to use
```javascript
var processPositionsString = require('simple-position-chooser').processPositionsString;

//The rule list can be a string or an array
var processor=processPositionsString('1,5,/4,13-17');
//or
var processor=processPositionsString([1,5,'/4','13-17']);

// Note: The end value is excluded
//processor(start,end)

processor(0,30)
//generates [ 0, 1, 4, 5, 8, 12, 13, 14, 15, 16, 17, 20, 24, 28 ]

processor(15,40)
//generates [ 15, 16, 17, 20, 24, 28, 32, 36 ]
```

### Example
```javascript
// In this example 

var array

var processPositionsString = require('simple-position-chooser').processPositionsString;

//The rule returns all possitions from 0 to 10 and all multiples of 5
var processor=processPositionsString('-10,/5');

//goes trough all the array positions that are between 0 and 10 or are multiple of 5
processor(0,array.length).forEach(function(pos){
    var element=array[pos]
    // code
})
```